import { render, screen } from '@testing-library/react';
import App from './app';

test('renders Résumé title', () => {
  render(<App />);
  const linkElement = screen.getByText(/Résumé/);
  expect(linkElement).toBeInTheDocument();
});
