# Olivier SECRET Résumé

> This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## npm scripts

- Start the developing server  
  `npm start`  
  Open [http://localhost:3000](http://localhost:3000) to view it in the browser.  

- Launches the test runner in the interactive watch mode.  
  `npm test`

- Builds the app for production.    
  `npm run build`
